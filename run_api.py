# Test Server.
from api import create_api, db
from os import environ
from flask_migrate import Migrate


if environ.get('FLASK_ENV') is None:
    print('FLASK_ENV not set')
app = create_api(environ.get('FLASK_ENV') or 'development')
migrate = Migrate(app, db)

with app.app_context():
    # db.reflect()
    # db.drop_all()
    db.create_all()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5200)
