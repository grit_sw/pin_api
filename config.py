import os


class Config(object):
	DEBUG = False
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	SECRET_KEY = os.urandom(12)
	ACCOUNT_URL = os.environ['ACCOUNT_URL']
	SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
	ADMINS = ['workshop@grit.systems']

	MAIL_SERVER = 'secure46.webhostinghub.com'
	MAIL_PORT = 465
	MAIL_USE_TLS = False
	MAIL_USE_SSL = True
	MAIL_USERNAME = 'gtg@grit.systems'
	MAIL_PASSWORD = '01*CqgBD@Oo2*T'

	TWILIO_ACCOUNT_SID = 'AC904e8b0c8540d6b7bd775cadb95127d1'
	TWILIO_AUTH_TOKEN = 'fa9355eda21386f01afa85c45afc3fef'
	TWILIO_MOBILE = '+17175370660'

	@staticmethod
	def init_app(app):
		pass


class Development(Config):
	DEBUG = True
	# SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'sqlite://')
	SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

	PIN_LENGTH = os.environ.get('PIN_LENGTH', 12)

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)

	@staticmethod
	def init_app(app, *args):
		pass


class Testing(Config):
	"""TODO"""
	pass


class Staging(Config):
	"""TODO"""
	PIN_LENGTH = os.environ.get('PIN_LENGTH', 12)
	SECRET_KEY = os.environ['SECRET_KEY']
	pass


class Production(Config):
	PIN_LENGTH = os.environ.get('PIN_LENGTH', 12)
	SECRET_KEY = os.environ['SECRET_KEY']

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


config = {
	'development': Development,
	'testing': Testing,
	'Staging': Staging,
	'Production': Production,

	'default': Development,
}
