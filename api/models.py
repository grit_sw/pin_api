import arrow
from api import db
from api.exceptions import NotFoundError, ValidationError
from logger import logger


class PinModel(db.Model):
	id = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	pin = db.Column(db.String, nullable=True)
	matched = db.Column(db.Boolean, default=False)
	validated = db.Column(db.Boolean, default=False)
	revoked = db.Column(db.Boolean, default=False)
	reference = db.Column(db.String, nullable=False)
	amount_paid = db.Column(db.Float, nullable=False)
	energy_units_purchased = db.Column(db.Integer, nullable=True)
	date_created = db.Column(db.DateTime, nullable=False)
	date_validated = db.Column(db.DateTime, nullable=True)
	owner_id = db.Column(db.Integer, db.ForeignKey("pin_user.id"))
	meter_id = db.Column(db.Integer, db.ForeignKey("meter.id"))

	def __init__(self, pin, reference, amount_paid, energy_units_purchased):
		self.pin = pin
		self.reference = reference
		self.amount_paid = amount_paid
		self.energy_units_purchased = energy_units_purchased
		# self.owner = owner
		self.validated = False
		self.revoked = False
		self.matched = False
		self.date_created = arrow.now().datetime

	@staticmethod
	def create(pin, reference, amount_paid, energy_units_purchased, owner, meter):
		new_pin = PinModel(
			pin=pin,
			reference=reference,
			amount_paid=amount_paid,
			energy_units_purchased=energy_units_purchased,
			# owner=owner,
			)
		owner.pins.append(new_pin)
		meter.pins.append(new_pin)
		db.session.add(new_pin)
		db.session.add(owner)
		return new_pin

	@staticmethod
	def get_reference(reference_id):
		return PinModel.query.filter_by(reference=reference_id).first()

	def get_data(self):
		return self.to_dict()

	def to_dict(self):
		data = {
			"pin": self.pin,
			"reference": self.reference,
			"group_id": self.owner.group_id,
			"meter_id": self.meter.meter_id,
			"amount_paid": self.amount_paid,
			"energy_units_purchased": self.energy_units_purchased,
			"matched": self.matched,
			"validated": self.validated,
			"date_created": str(self.date_created),
			"date_validated": str(self.date_validated),
		}
		return data


class Meter(db.Model):
	id = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	meter_id = db.Column(db.String, unique=True, nullable=False)
	group_id = db.Column(db.Integer, db.ForeignKey("pin_user.id"))
	pins = db.relationship("PinModel", backref="meter", lazy="dynamic")

	def __init__(self, meter_id):
		self.meter_id = meter_id

	@staticmethod
	def create(meter_id):
		new_meter = Meter(
			meter_id=meter_id
		)
		db.session.add(new_meter)
		return new_meter

	@staticmethod
	def get_one(meter_id):
		return Meter.query.filter_by(meter_id=meter_id).first()

	@staticmethod
	def get_owner(meter_id):
		meter = Meter.query.filter_by(meter_id=meter_id).first()
		if not hasattr(meter, 'user'):
			return None
		return meter.user


class PinUser(db.Model):
	id = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	group_id = db.Column(db.String, unique=False, nullable=False)
	pins = db.relationship("PinModel", backref="owner", lazy="dynamic")
	meters = db.relationship("Meter", backref="user", lazy="dynamic")

	def __init__(self, group_id):
		self.group_id = group_id

	@staticmethod
	def create_no_commit(group_id, meter_id):
		user = PinUser(group_id)
		db.session.add(user)
		meter = Meter.get_one(meter_id)
		if meter:
			old_user = meter.user
			old_user.meters.remove(meter)
			db.session.add(old_user)
			user.meters.append(meter)
		else:
			meter = Meter.create(meter_id)
			user.meters.append(meter)
		db.session.add(user)
		return user, meter

	@staticmethod
	def get_user(group_id=None, meter_id=None):
		if not any([group_id, meter_id]):
			return None
		if group_id:
			user = PinUser.query.filter_by(group_id=group_id).first()
		elif meter_id:
			user = Meter.get_owner(meter_id)
		return user

	@staticmethod
	def get_meter_user(group_id, meter_id):
		if not any([group_id, meter_id]):
			return None
		meter_user = PinUser.query.filter_by(group_id=group_id)\
			.join(Meter).filter_by(meter_id=meter_id).first()
		print(meter_user)
		return meter_user

	@staticmethod
	def get_user_or_create_no_commit(group_id):
		user = PinUser.get_user(group_id=group_id)
		if not user:
			user = PinUser(group_id)
			db.session.add(user)
		return user

	@staticmethod
	def set_pin(pin_params):
		group_id = pin_params["group_id"]
		meter_id = pin_params["meter_id"]
		del pin_params["group_id"]
		del pin_params["meter_id"]
		meter_user = PinUser.get_meter_user(group_id=group_id, meter_id=meter_id)
		meter = Meter.get_one(meter_id)
		if not meter_user:
			meter_user, meter = PinUser.create_no_commit(group_id, meter_id)
		pin_params["owner"] = meter_user
		pin_params["meter"] = meter
		new_pin = PinModel.create(**pin_params)
		db.session.commit()
		db.session.refresh(new_pin)
		logger.info(meter_user.pins.filter_by(validated=False).all())
		logger.info("NEW PIN {}".format(new_pin.to_dict()))
		return new_pin.to_dict()

	# @staticmethod
	# def set_pin(pin_params):
	# 	"""Method that saves the generated pin in the database"""
	# 	group_id = pin_params["group_id"]
	# 	meter_id = pin_params["meter_id"]
	# 	del pin_params["group_id"]
	# 	del pin_params["meter_id"]
	# 	meter_user = PinUser.get_meter_user(group_id=group_id, meter_id=meter_id)
	# 	user = PinUser.get_user(group_id=group_id)
	# 	meter = Meter.get_one(meter_id)
	# 	if meter:
	# 		if meter.user != user:
	# 			raise UserNotAssignedThisMeter("This meter is assigned to another user")
	# 	if meter_user is None:
	# 		# Please note that in this implementation, the meter ID is not unique to users.
	# 		# The problem with this is that if a meter, METER A is reassigned from USER A to USER B,
	# 		# USER A will still be associated with METER A.
	# 		# The only association that USER A can have with the METER A is for viewing previous PINS.
	# 		meter_user, meter = PinUser.create_no_commit(group_id, meter_id)
	# 	pin_params["owner"] = meter_user
	# 	pin_params["meter"] = meter
	# 	new_pin = PinModel.create(**pin_params)
	# 	db.session.commit()
	# 	db.session.refresh(new_pin)
	# 	logger.info(meter_user.pins.filter_by(validated=False).all())
	# 	logger.info("NEW PIN {}".format(new_pin.to_dict()))
	# 	return new_pin.to_dict()

	@staticmethod
	def get_pins(group_id, validated=False):
		user = PinUser.get_user(group_id)
		if user is None:
			raise NotFoundError("User {} not found".format(group_id))
		data = {}
		data["group_id"] = group_id
		user_pins = user.pins.filter_by(validated=validated).all()
		pin_count = len(user_pins)
		pins = [pin.to_dict() for pin in user_pins]
		logger.info(user_pins)
		data["pin_count"] = pin_count
		data["pins"] = pins
		return data

	@staticmethod
	def get_pin_list(group_id, meter_id, validated=False):
		meter_user = PinUser.get_meter_user(group_id, meter_id)
		if meter_user is None:
			raise NotFoundError("No meters assigned to this user")
		user_pins = meter_user.pins.filter_by(validated=validated).all()
		if not user_pins:
			raise NotFoundError("No pins for this meter_user")
		pins = [user_pin.pin for user_pin in user_pins]
		return pins

	def check_pin(self, input_pin):
		"""Method that checks if the supplied pin is correct"""
		# This query ensures that the pin that"s being checked is assigned to a user group
		# or a meter.
		input_pin = str(input_pin)
		pin_model = PinModel.query.filter_by(pin=input_pin).first()

		if not pin_model:
			resp = "Invalid Pin."
			return False, False, resp

		if not hasattr(pin_model, "owner"):
			logger.critical("Pin {} not assigned to any meter or owner")
			resp = "Invalid Pin."
			return False, False, resp

		if pin_model.validated:
			resp = pin_model.to_dict()
			reused = True
			return False, reused, resp

		# todo: Delete pin after a year
		pin_model.matched = True
		pin_model.validated = True
		pin_model.date_validated = arrow.now().datetime
		db.session.add(pin_model)
		db.session.commit()
		db.session.refresh(pin_model)

		resp = pin_model.to_dict()

		reused = False
		return True, reused, resp

	@staticmethod
	def set_validate_flag(group_id, input_pin):

		user = PinUser.get_user(group_id)
		if user is None:
			raise NotFoundError("User {} not found".format(group_id))

		pin_model = user.pins.filter_by(pin=input_pin).first()

		if pin_model is None:
			raise NotFoundError("Pin {} not found".format(input_pin))
		if pin_model.validated:
			raise ValidationError(pin_model.to_dict())
		pin_model.validated = True
		pin_model.date_validated = arrow.now().datetime
		db.session.add(pin_model)
		db.session.commit()

	def get_validated_references(self):
		return [pin.reference for pin in self.pins.filter_by(validated=True).all()]

	@staticmethod
	def delete_pin(group_id, pin):
		"""
			Method for deleting a pin
		"""
		# self.revoked = True
		# self.pin = None
		user = PinUser.get_user(group_id)
		if user is None:
			raise NotFoundError("User {} not found".format(group_id))

		pin_model = user.pins.filter_by(pin=pin).first()
		db.session.delete(pin_model)
		db.session.commit()
