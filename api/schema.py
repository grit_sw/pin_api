from collections import namedtuple

import arrow
from marshmallow import Schema, ValidationError
from marshmallow import fields as ma_fields
from marshmallow import post_load, validates

CreatePin = namedtuple('CreatePin', [
	'reference',
	'group_id',
	'mobile',
	'email',
	'full_name',
	'meter_id',
	'amount_paid',
	'energy_units_purchased',
])

CreatePin.__new__.__defaults__ = (None,) * len(CreatePin._fields)

ValidatePin = namedtuple('ValidatePin', [
	'group_id',
	'pin',
	'meter_id',
])


Email = namedtuple('Email', [
	'group_id',
	'receiver_email',
	'full_name',
])


Mobile = namedtuple('Mobile', [
	'group_id',
	'receiver_mobile',
	'full_name',
])


class CreatePinSchema(Schema):
	group_id = ma_fields.String(required=True)
	reference = ma_fields.String(required=True)
	mobile = ma_fields.String(required=True)
	email = ma_fields.String(required=True)
	full_name = ma_fields.String(required=True)
	amount_paid = ma_fields.Float(required=True)
	meter_id = ma_fields.String(required=False)
	energy_units_purchased = ma_fields.Integer(required=False)

	@post_load
	def new_pin(self, data):
		return CreatePin(**data)

	@validates('group_id')
	def validate_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Group ID required')

	@validates('reference')
	def validate_reference(self, value):
		if len(value) <= 0:
			raise ValidationError('Reference ID required')

	@validates('mobile')
	def validate_mobile(self, value):
		if len(value) <= 0:
			raise ValidationError('Phone number required')

	@validates('email')
	def validate_email(self, value):
		if len(value) <= 0:
			raise ValidationError('Email required')

	@validates('full_name')
	def validate_full_name(self, value):
		if len(value) <= 0:
			raise ValidationError('Full name required')

	@validates('amount_paid')
	def validate_amount_paid(self, value):
		try:
			float(value)
		except Exception:
			raise ValidationError('Amount paid required.')

	def __repr__(self):
		return "CreatePinSchema <{}>".format(self.group_id)


class EmailSchema(Schema):
	group_id = ma_fields.String(required=True)
	receiver_email = ma_fields.Email(required=True)
	full_name = ma_fields.String(required=True)

	@post_load
	def new_email(self, data):
		return Email(**data)

	@validates('group_id')
	def validate_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Value cannot be empty')

	@validates('receiver_email')
	def validate_receiver_email(self, value):
		if len(value) <= 0:
			raise ValidationError('Value cannot be empty')

	@validates('full_name')
	def validate_full_name(self, value):
		if len(value) <= 0:
			raise ValidationError('Value cannot be empty')

	def __repr__(self):
		return "EmailSchema <{}>".format(self.group_id)


class MobileSchema(Schema):
	group_id = ma_fields.String(required=True)
	receiver_mobile = ma_fields.String(required=True)
	full_name = ma_fields.String(required=True)

	@post_load
	def new_sms(self, data):
		return Mobile(**data)

	@validates('group_id')
	def validate_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Value cannot be empty')

	@validates('receiver_mobile')
	def validate_receiver_mobile(self, value):
		if len(value) <= 0:
			raise ValidationError('Value cannot be empty')

	@validates('full_name')
	def validate_full_name(self, value):
		if len(value) <= 0:
			raise ValidationError('Value cannot be empty')

	def __repr__(self):
		return "EmailSchema <{}>".format(self.group_id)


class ValidationSchema(Schema):
	group_id = ma_fields.String(required=True, allow_none=True)
	meter_id = ma_fields.String(required=True)
	pin = ma_fields.String(required=True)

	@post_load
	def validate(self, data):
		return ValidatePin(**data)

	@validates('meter_id')
	def validate_meter_id(self, value):
		if not value:
			raise ValidationError('Meter ID required.')

	@validates('pin')
	def validate_pin(self, value):
		if len(value) <= 0:
			raise ValidationError('Value cannot be empty')
