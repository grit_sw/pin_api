from flask_mail import Message
from api import app
from threading import Thread
from flask import render_template
from logger import logger
from twilio.rest import Client


def send_async_pin(app, msg):
	with app.app_context():
		try:
			mail.send(msg)
			logger.info('Sent e-mail to {}'.format(msg.recipients))
		except Exception as e:
			logger.exception(e)
			pass


def send_sms(subject, sender_mobile, receiver_mobile, text_body):
	# Your Account Sid and Auth Token from twilio.com/console
	account_sid = app.config['TWILIO_ACCOUNT_SID']
	auth_token = app.config['TWILIO_AUTH_TOKEN']
	client = Client(account_sid, auth_token)

	message = client.messages.create(
								body=text_body,
								from_=sender_mobile,
								to=receiver_mobile
							)

	print("\n\n\nmessage.sid")
	print(message.sid)

	# Thread(target=send_async_pin, args=(app, msg)).start()


def send_pin_sms(greeting, receiver_mobile, full_name, pin_conjunction, pin_plurality, pin_list):

	send_sms(
		subject='[GTG] Your Energy Pins',
		sender_mobile=app.config['TWILIO_MOBILE'],
		receiver_mobile=receiver_mobile,
		text_body=render_template(
			'pin_sms.txt',
			greeting=greeting,
			full_name=full_name,
			pin_conjunction=pin_conjunction,
			pin_plurality=pin_plurality,
			pin_list=pin_list,
		),
	)
