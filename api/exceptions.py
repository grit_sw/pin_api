class NotFoundError(AttributeError):
	"""Inheriting from AttributError since sqlalchemy returns None if a user is not found"""
	pass


class DuplicateError(ValueError):
	"""Raised if multiple code generation requests are made"""
	pass


class ValidationError(ValueError):
	"""docstring for ValidationError"""
	pass

class UserNotAssignedThisMeter(ValueError):
	"""docstring for UserNotAssignedThisMeter"""
	pass
