from flask import request
from flask_restplus import Namespace, Resource, fields
from api.manager import PinManager
from api import api
from api.schema import CreatePinSchema, ValidationSchema, EmailSchema, MobileSchema
from logger import logger
from marshmallow import ValidationError

pin_api = Namespace('pins', description='electricity pins related operations')

create_pin = pin_api.model('Pin Creation', {
	'reference': fields.String(required=True, description='Reference code for the transaction.'),
	'group_id': fields.String(required=True, description='ID of the paying user.'),
	'mobile': fields.String(required=True, description='Phone number of the paying user.'),
	'email': fields.String(required=True, description='Email address of the paying user.'),
	'full_name': fields.String(required=True, description='Full name of the paying user.'),
	'meter_id': fields.String(required=True, description='Meter ID the paying user.'),
	'amount_paid': fields.Float(required=True, description='Amount of money paid by the user.'),
	'energy_units_purchased': fields.Integer(required=True, description='Energy units purchased by the user.'),
})

send_email = pin_api.model('Email Sender', {
	'group_id': fields.String(required=True, description='Group ID of the user.'),
	'receiver_email': fields.String(required=True, description='Email to send the pin to.'),
	'full_name': fields.String(required=True, description='Full name of the user.'),
})

send_sms = pin_api.model('SMS Sender', {
	'group_id': fields.String(required=True, description='Group ID of the user.'),
	'receiver_mobile': fields.String(required=True, description='Phone number to send the pin to.'),
	'full_name': fields.String(required=True, description='Full name of the user.'),
})

validate_pin = pin_api.model('Pin Validation', {
	'pin': fields.String(required=True, description='Pin to be validated.'),
	'meter_id': fields.String(required=True, description='Meter ID of the to be validated'),
})


@pin_api.route('/new/')
class PinApi(Resource):
	"""
		Api route for managing pin generation and validation
		@args: payload
		@returns: response and status code
	"""
	@pin_api.expect(create_pin)
	def post(self):
		'''HTTP method to generate a pin'''
		data = request.values.to_dict()
		payload = api.payload or data
		manager = PinManager()
		schema = CreatePinSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['message'] = 'Invalid fields.'
			return response, 400
		resp, code = manager.create_pin(new_payload)
		return resp, code


@pin_api.route('/validate/')
class Validate(Resource):
	"""docstring for Validate"""

	@pin_api.expect(validate_pin)
	def post(self):
		'''HTTP method to validate pin'''
		data = request.values.to_dict()
		payload = api.payload or data
		manager = PinManager()
		schema = ValidationSchema(strict=True)

		group_id = request.cookies.get('group_id')
		payload['group_id'] = group_id

		try:
			validation_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['message'] = 'Invalid Fields'
			return response, 400

		resp, code = manager.validate_pin(**validation_payload)
		return resp, code


@pin_api.route('/send-mail/')
class EmailPin(Resource):
	"""
		Api route for sending emails to individual users.
		Used by a GRIT, management or client admin to view a user's pin history.
	"""

	@pin_api.expect(send_email)
	def post(self):
		response = {}
		data = request.values.to_dict()
		payload = api.payload or data
		manager = PinManager()
		schema = EmailSchema(strict=True)

		role_id = request.cookies.get('role_id')

		try:
			email_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response['success'] = False
			response['message'] = 'Invalid Fields'
			return response, 400

		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourised'
			return response, 403

		try:
			int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised'
			return response, 403

		resp, code = manager.send_pin(email=True, **email_payload)
		return resp, code


@pin_api.route('/send-sms/')
class SMSPin(Resource):
	"""
		Api route for sending pins to users via text message.
		Used by a grit, management or client admin to view a user's pin history.
	"""

	@pin_api.expect(send_sms)
	def post(self):
		response = {}
		data = request.values.to_dict()
		payload = api.payload or data
		manager = PinManager()
		schema = MobileSchema(strict=True)

		role_id = request.cookies.get('role_id')
		payload['role_id'] = role_id

		try:
			sms_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['message'] = 'Invalid Fields'
			return response, 400

		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourised'
			return response, 403

		try:
			int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised'
			return response, 403

		resp, code = manager.send_pin(sms=True, **sms_payload)
		return resp, code


@pin_api.route('/user-pin/<string:group_id>/')
class UserPin(Resource):
	"""
		Api route for managing individual users
		Used by a clinet admin to view a user's pin history.
		Returns both used and unused pins
	"""

	def get(self, group_id):
		manager = PinManager()
		viewer_role = request.cookies.get('role_id')
		resp, code = manager.view_pin(group_id, viewer_role, validated=True)
		return resp, code


@pin_api.route('/my-pins/')
class MyPin(Resource):
	"""
		Used by an individual to see their avalable pins.
		Only shows unused pins
	"""

	def get(self):
		manager = PinManager()
		group_id = request.cookies.get('group_id')
		viewer_role = int(request.cookies.get('role_id'))
		resp, code = manager.view_pin(group_id, viewer_role)
		return resp, code


@pin_api.route('/validated-references/')
class ValidatedReferences(Resource):
	"""
		Used by an individual to see their avalable pins.
		Only shows unused pins
	"""

	def get(self):
		manager = PinManager()
		group_id = request.cookies.get('group_id')
		resp, code = manager.get_validated_references(group_id)
		return resp, code


@pin_api.route('/revoke_pin/')
class PinRevocationApi(Resource):
	"""
		Pin revocation api
		To be used by an admin to change the pin issued to a user
		or
		To be used by a user to change the issued pin in the event they
		misplace the pin originally issued to them
		@args: payload
		@returns: response and status code
		The method is already implemented in PinManager
		see PinManager.revoke_pin
		Its position security-wise has not been figured out yet
	"""
	pass
