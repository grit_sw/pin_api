from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from config import config
from flask_restplus import Api
from flask_migrate import Migrate
from flask_mail import Mail


db = SQLAlchemy()
api = Api(doc='/doc/')
migrate = Migrate()
mail = Mail()

app = Flask(__name__)


def create_api(config_name):
    try:
        init_config = config[config_name]()
    except KeyError:
        raise
    except Exception:
        # For unforseen exceptions
        raise
        exit()

    print('Running in {} Mode'.format(init_config))
    config_object = config.get(config_name)

    app.config.from_object(config_object)

    db.init_app(app)
    mail.init_app(app)
    migrate.init_app(app)

    from api.controllers import pin_api as ns1
    api.add_namespace(ns1, path='/pin')

    api.init_app(app)

    return app
