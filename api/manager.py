from secrets import SystemRandom

import arrow
from flask import current_app

from api.emails import send_pin_email
from api.exceptions import NotFoundError
from api.models import PinModel, PinUser
from api.sms import send_pin_sms
from logger import logger


def generate_pins(number_of_digits):
	"""
		Function that generates the pins during pin creation
		@args: number of digits
		@returns: generated pin
	"""
	return int(''.join(str(SystemRandom().randint(0, 9)) for _ in range(int(number_of_digits))))


def email_params(pin_list):
	if len(pin_list) > 1:
		pin_conjunction = 'are'
		pin_plurality = 'pins'
	else:
		pin_conjunction = 'is'
		pin_plurality = 'pin'
	current_time = arrow.now('Africa/Lagos').hour
	if current_time < 12:
		greeting = 'Good morning'
	elif 12 < current_time < 16:
		greeting = 'Good afternoon'
	else:
		greeting = 'Good evening'

	params = {}
	params['pin_conjunction'] = pin_conjunction
	params['pin_plurality'] = pin_plurality
	params['greeting'] = greeting
	return params


class PinManager(object):
	"""Class to manage pin creation, validation and revocation"""

	def __init__(self):

		try:
			self.length = current_app.config['PIN_LENGTH']
			self.account_api = current_app.config['ACCOUNT_URL']
		except KeyError as e:
			logger.exception(e)
			raise

	def create_pin(self, payload):
		"""
			Method to create pins
			@args: data payload, length of pin
			@returns: data payload with the pin and status code
		"""
		response = {}
		reference = payload["reference"]
		group_id = payload["group_id"]
		meter_id = payload["meter_id"]

		mobile = payload["mobile"]
		email = payload["email"]
		full_name = payload["full_name"]

		del payload["mobile"]
		del payload["email"]
		del payload["full_name"]

		existing_pin = PinModel.get_reference(reference)
		if existing_pin:
			response['success'] = True
			response['message'] = 'Pin already generated for this reference.'
			response['data'] = existing_pin.get_data()
			return response, 409

		pin = generate_pins(self.length)
		payload['pin'] = pin
		data = PinUser.set_pin(payload)

		logger.info(f"Pin {data['pin']} Generated for {group_id}")
		self.send_pin(
			group_id=group_id,
			meter_id=meter_id,
			full_name=full_name,
			receiver_email=email,
			receiver_mobile=mobile,
			email=True,
			sms=True,
		)
		response['success'] = True
		response['data'] = data
		return response, 201

	def view_pin(self, group_id, role_id, validated=False):
		"""
			Method to view a user's pins
			@args: data payload
			@returns: None
		"""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			response['success'] = False
			response['message'] = "Unauthorized"

		if validated:
			if role_id != 5:
				response['success'] = False
				response['message'] = 'Unauthourized user'
				return response, 403

		try:
			data = PinUser.get_pins(group_id, validated=validated)
			response['success'] = True
			response['data'] = data
			return response, 200

		except NotFoundError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "User not found"
			return response, 404

	def validate_pin(self, meter_id, pin, group_id=None):
		"""
			Method to validate pin input.
			@args: data payload
			@returns: bool and status code
		"""

		response = {}
		if not any([group_id, meter_id]):
			response['success'] = False
			response['message'] = 'User Group ID or Meter ID is required.'
			return response, 400

		if not group_id:
			meter_user = PinUser.get_user(meter_id=meter_id)
			logger.info(f"GROUP ID = {group_id}, METER ID = {meter_id}")
		else:
			logger.info(f"GROUP ID = {group_id}, METER ID = {meter_id}")
			meter_user = PinUser.get_meter_user(group_id=group_id, meter_id=meter_id)
		if not meter_user:
			logger.info(f"GROUP ID = {group_id}, METER ID = {meter_id}")
			response['success'] = False
			response['message'] = 'User not found'
			return response, 404

		validated, reused, pin_info = meter_user.check_pin(pin)

		if reused:
			response['success'] = True
			response['data'] = pin_info
			return response, 409

		if validated:
			response['success'] = True
			response['data'] = pin_info
			return response, 200

		else:
			response['success'] = False
			response['message'] = pin_info
			return response, 400

	def revoke_pin(self):
		"""
			ALERT!!!! URL FOR THIS HAS NOT BEEN IMPLEMENTED
			Method to delete pins
			@args: data payload
			@returns: None
		"""
		# todo: delete all pins for all users greater than one year.
		return {}, 501

	def send_pin(self, group_id, meter_id, full_name, receiver_email=None, receiver_mobile=None, email=False, sms=False):
		"""
			Method to view a user's pins
			@args: data payload
			@returns: None
		"""
		response = {}

		try:
			pin_list = PinUser.get_pin_list(group_id, meter_id)
			params = email_params(pin_list)
			greeting = params['greeting']
			pin_conjunction = params['pin_conjunction']
			pin_plurality = params['pin_plurality']
			if email:
				try:
					send_pin_email(
						greeting=greeting,
						full_name=full_name,
						receiver_email=receiver_email,
						pin_conjunction=pin_conjunction,
						pin_plurality=pin_plurality,
						pin_list=pin_list
					)
					message = 'Email sent to {}'.format(receiver_email)
				except Exception as e:
					logger.critical(e)
					response['success'] = False
					response['message'] = 'Error when sending email'
					return response, 400

			if sms:
				try:
					send_pin_sms(
						greeting=greeting,
						full_name=full_name,
						receiver_mobile=receiver_mobile,
						pin_conjunction=pin_conjunction,
						pin_plurality=pin_plurality,
						pin_list=pin_list
					)
					message = 'Text message sent to {}'.format(receiver_mobile)
				except Exception as e:
					logger.critical(e)
					response['success'] = False
					response['message'] = 'Error when sending text message.'
					return response, 400

			response['success'] = True
			response['message'] = message
			return response, 200

		except NotFoundError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = e.args[0]
			return response, 404

		except ValueError as e:
			logger.info("No pending pins for user group {} found.".format(group_id))
			response['success'] = False
			response['message'] = 'No pending pins for this user.'
			return response, 404

	def get_validated_references(self, group_id):
		response = {}
		user = PinUser.get_user(group_id=group_id)
		if not user:
			response['success'] = False
			response['message'] = "User not found."
			return response, 404

		references = user.get_validated_references()
		response['success'] = True
		response['data'] = references
		return response, 200
