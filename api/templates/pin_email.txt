{{greeting}} {{full_name}},

    Here {{pin_conjunction}} your energy {{pin_plurality}}.
    {% for pin in pin_list %}
        {{pin}}
    {% endfor %}

Regards,
The GTG Team.
