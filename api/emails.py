from flask_mail import Message
from api import mail, app
from threading import Thread
from flask import render_template
from logger import logger


def send_async_pin(app, msg):
	with app.app_context():
		try:
			mail.send(msg)
			logger.info('Sent e-mail to {}'.format(msg.recipients))
		except Exception as e:
			logger.exception(e)
			pass


def send_email(subject, sender, recipients, text_body, html_body):
	msg = Message(subject, sender=sender, recipients=recipients)
	msg.body = text_body
	msg.html = html_body

	Thread(target=send_async_pin, args=(app, msg)).start()


def send_pin_email(greeting, receiver_email, full_name, pin_conjunction, pin_plurality, pin_list):

	send_email(
		subject='[GTG] Your Energy Pins',
		sender=app.config['ADMINS'][0],
		recipients=[receiver_email],
		text_body=render_template(
			'pin_email.txt',
			greeting=greeting,
			full_name=full_name,
			pin_conjunction=pin_conjunction,
			pin_plurality=pin_plurality,
			pin_list=pin_list,
		),
		html_body=render_template(
			'pin_email.html',
			greeting=greeting,
			full_name=full_name,
			pin_conjunction=pin_conjunction,
			pin_plurality=pin_plurality,
			pin_list=pin_list,
		),
	)
