from requests import post
from arrow import now


payload = {
    'pin': 932278437920,
    'validated': False,
    'meter_id': 'B8:27:EB:8V:MM:Q6',
    'customer_id': 'B8:27:EB:8V:MM:Q6-04',
    'customer_fid': '2345',
    'amount_paid': '2321',
    'currency': 'Naira',
    'plan_name': 'Gold',
    'message': 'Need Pin',
    'meter_connected': False,
    'time': str(now())
}

# url = 'http://0.0.0.0:5100/pins/generate_pin'
url = 'http://0.0.0.0:5100/pins/validate_pin'
r = post(url, data=payload)
print(r.text)
print(dir(r))
